[![pipeline status](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-cellpose/badges/main/pipeline.svg)](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-cellpose/-/commits/main)
[![Latest Release](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-cellpose/-/badges/release.svg)](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-cellpose/-/releases)


# singularity-cellpose

This is the DCGC singularity recipe for running [cellpose](https://github.com/MouseLand/cellpose).


## How to build

```bash
singularity build -f -F singularity-cellpose.sif Singularity
```

## How to run

Run cellpose as command line tool:

```bash
singularity run --writable-tmpfs --app cellpose singularity-cellpose.sif ...
```

Run cellpose via GUI:

```bash
singularity run --writable-tmpfs --app cellpose_gui singularity-cellpose.sif
```

Extract a level of details from a morphology.ome.tif file:

```
singularity run -B /projects --app extract_tiff singularity-cellpose.sif --level 2 morphology.ome.tif level2_morphology.ome.tif
```

Extract image information from a morphology.ome.tif file:

```
singularity run -B /projects --app tiff_info singularity-cellpose.sif morphology.ome.tif
```
