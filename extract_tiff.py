#!/usr/bin/env python

# Adapted from https://www.10xgenomics.com/analysis-guides/performing-3d-nucleus-segmentation-with-cellpose-and-generating-a-feature-cell-matrix

from os.path import basename
import argparse

import tifffile

# Parse arguments
parser = argparse.ArgumentParser(
                    prog='extract_tiff.py',
                    description='Extracts a single details level from a morphology.ome.tif file. Typically ranges from 0 (original image with highest resolution) to 7 (lowest resolution).')

parser.add_argument('tiff_infile')
parser.add_argument('tiff_outfile')
parser.add_argument('--level', required=True)
args = parser.parse_args()

# Variable 'LEVEL' determines the level to extract. It ranges from 0 (highest resolution) to 7 (lowest resolution) for morphology.ome.tif
infilename = args.tiff_infile
level = int(args.level)
outfilename = args.tiff_outfile

with tifffile.TiffFile(infilename) as tif:
    image = tif.series[0].levels[level].asarray()

tifffile.imwrite(
    outfilename,
    image,
    photometric='minisblack',
    dtype='uint16',
    tile=(1024, 1024),
    compression='JPEG_2000_LOSSY',
    metadata={'axes': 'ZYX'},
)
