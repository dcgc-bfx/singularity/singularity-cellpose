#!/usr/bin/env python

# Adapted from https://www.10xgenomics.com/analysis-guides/performing-3d-nucleus-segmentation-with-cellpose-and-generating-a-feature-cell-matrix

from os.path import basename
import argparse

import tifffile

# Parse arguments
parser = argparse.ArgumentParser(
                    prog='tiff_info.py',
                    description='Prints a description of a tiff image.')

parser.add_argument('tiff_infile')
args = parser.parse_args()

tiff_infile = args.tiff_infile

with tifffile.TiffFile(tiff_infile) as tif:
    for tag in tif.pages[0].tags.values():
        if tag.name == "ImageDescription":
            print(tag.name+":", tag.value)
