Bootstrap: docker
From: mambaorg/micromamba:bullseye

%labels
    Author andreas.petzold@tu-dresden.de
    Organisation DcGC

%help
  Container for running cellpose.

  Run cellpose as command line tool:
  singularity run --writable-tmpfs --app cellpose singularity-cellpose.sif -h

  Start cellpose GUI:
  singularity run --writable-tmpfs --app cellpose_gui singularity-cellpose.sif

%files
  .bashrc
  extract_tiff.py /opt/conda/bin/extract_tiff.py
  tiff_info.py /opt/conda/bin/tiff_info.py

%environment
  export DEBIAN_FRONTEND=noninteractive
  export PATH=/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  export LD_LIBRARY_PATH=/opt/conda/lib:/opt/conda/lib/server
  export NUMBA_CACHE_DIR=/tmp

%post
  set -eu

  # Build date (when building, pass via SINGULARITYENV_CONTAINER_BUILD_DATE otherwise date when building)
  if [ -z ${CONTAINER_BUILD_DATE+x} ]
  then
    CONTAINER_BUILD_DATE=$(date)
  fi
  echo "export CONTAINER_BUILD_DATE=\"${CONTAINER_BUILD_DATE}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository names (when building, pass via SINGULARITYENV_CONTAINER_GIT_NAME otherwise empty)
  if [ -z ${CONTAINER_GIT_NAME+x} ]
  then
    CONTAINER_GIT_NAME=''
  fi
  echo "export CONTAINER_GIT_NAME=\"${CONTAINER_GIT_NAME}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository url (when building, pass via SINGULARITYENV_CONTAINER_GIT_URL otherwise empty)
  if [ -z ${CONTAINER_GIT_URL+x} ]
  then
    CONTAINER_GIT_URL=''
  fi
  echo "export CONTAINER_GIT_URL=\"${CONTAINER_GIT_URL}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository commit id (when building, pass via SINGULARITYENV_CONTAINER_GIT_COMMIT_ID otherwise empty)
  if [ -z ${CONTAINER_GIT_COMMIT_ID+x} ]
  then
    CONTAINER_GIT_COMMIT_ID=''
  fi
  echo "export CONTAINER_GIT_COMMIT_ID=\"${CONTAINER_GIT_COMMIT_ID}\"" >> $SINGULARITY_ENVIRONMENT

  # Container version (when building, pass via SINGULARITYENV_CONTAINER_VERSION otherwise empty)
  if [ -z ${CONTAINER_VERSION+x} ]
  then
    CONTAINER_VERSION=''
  fi
  echo "export CONTAINER_VERSION=\"${CONTAINER_VERSION}\"" >> $SINGULARITY_ENVIRONMENT

  # the linux command 'make' supports the compilation of independent targets in parallel; this is also passed to R 'install.packages' since it uses 'make' internally
  if [ -z ${CONTAINER_COMPILE_CPUS+x} ]
  then
    COMPILE_CPUS=6
  else
    COMPILE_CPUS="${CONTAINER_COMPILE_CPUS}"
  fi
  export COMPILE_CPUS

  # deactive interactive dialogs
  export DEBIAN_FRONTEND=noninteractive

  # install and reconfigure locale
  apt-get clean -q && apt-get update -y -q && apt-get upgrade -y -q
  apt-get install -y -q locales
  apt-get clean -q
  export LANGUAGE="en_US.UTF-8"
  export LANG="en_US.UTF-8"
  export LC_ALL="en_US.UTF-8"
  echo "LC_ALL=en_US.UTF-8" >> /etc/environment
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  echo "LANG=en_US.UTF-8" > /etc/locale.conf
  locale-gen --purge en_US.UTF-8
  dpkg-reconfigure --frontend=noninteractive locales

  # set PATH for use of conda/mamba; also add to system-wide /etc/profile
  export PATH=/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  echo >> /etc/profile
  echo 'export PATH=/opt/conda/bin:$PATH' >> /etc/profile
  echo 'export LD_LIBRARY_PATH=/opt/conda/lib:/opt/conda/lib/server:$LD_LIBRARY_PATH' >> /etc/profile

  # some packages via apt-get
  apt-get update --fix-missing -q
  apt-get install -y -q\
    build-essential \
    gawk \
    gdebi-core \
    git \
    libatlas-base-dev \
    libxrender1 \
    p7zip-full \
    unzip \
    uuid \
    wget \
    libncurses5-dev \
    libncursesw5-dev \
    libreadline8 \
    libtinfo6 \
    libgsl-dev \
    libssl1.1 \
    
# cellpose system requirements
apt-get install -y -q\
    libgl1 \
    libegl1 \
    libopengl0 \
    libxkbcommon-x11-0 \
    libxcb-cursor0 \
    libxcb-icccm4 \
    libxcb-shape0 \
    libxcb-keysyms1 \
    libglib2.0-0 \
    libdbus-1-3 \
    libgtk-3-0


# Base conda
micromamba install -y -n base -c conda-forge python=3.10 pkg-config pip mamba

# Tools needed for pre-processing Xenium data
micromamba install -y -n base -c conda-forge tifffile pyarrow pandas scipy

# Cellpose
CELLPOSE_VERSION="2.2.2"

micromamba install -n base -c conda-forge PySide6
/opt/conda/bin/pip install git+https://www.github.com/mouseland/cellpose.git@v${CELLPOSE_VERSION}#egg=cellpose[gui]

# Permissions for extrac_tiff.py and tiff_info.py
chmod +x /opt/conda/bin/extract_tiff.py /opt/conda/bin/tiff_info.py

%apprun cellpose
  bash <<-EOF
        source /.bashrc
        source activate /opt/conda
        cellpose ${@}
EOF

%apprun cellpose_gui
  bash <<-EOF
        source /.bashrc
        source activate /opt/conda
        python -m cellpose
EOF
        
%apprun extract_tiff
  bash <<-EOF
        source /.bashrc
        source activate /opt/conda
        python /opt/conda/bin/extract_tiff.py ${@}
EOF

%apprun tiff_info
  bash <<-EOF
        source /.bashrc
        source activate /opt/conda
        python /opt/conda/bin/tiff_info.py ${@}

EOF

